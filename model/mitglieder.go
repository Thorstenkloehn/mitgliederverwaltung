package model

type Mitglider struct {
	ID       uint64 `gorm:"primaryKey"`
	User     string
	Passwort string
	Emails   string
}
