package model

import (
	"time"
)

type Session struct {
	ID string `gorm:"primaryKey"`

	Mittgliederid uint
	Mitglieder    Mitglider `gorm:"foreignKey:Mittgliederid"`
	IPAdresse     string
	UserAgent     string
	Zuletzt       time.Time
}
