package mitgliederverwaltung

import (
	"github.com/spf13/viper"
	"github.com/thorstenkloehn/mitgliederverwaltung/model"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func Installieren() {

	dsn := viper.GetString("mysql_nutzername") + `:` + viper.GetString("mysql_passwort") + `@tcp(127.0.0.1:3306)/` + viper.GetString("Mysql_Datenbank_Name") + `?charset=utf8mb4&parseTime=True&loc=Local`
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("Datenbank kann nicht zugriffen werden")
	}
	db.AutoMigrate(&model.Mitglider{}, &model.Session{})

}
