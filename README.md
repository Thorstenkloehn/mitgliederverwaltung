# Mitgliederverwaltung

## Alle Mitglieder zeigen
```
var start = controller.Mitglieder{}
Nutzername := *start.Index()


```

## Nutzer Registieren

```
var start = controller.Mitglieder{}
	start.Rohepasswort = []byte("Thorsten")
	start.Mitglieder.User = "Thorsten"
	start.Mitglieder.Emails = "thorstenkloehn@gmail.com"
	fmt.Println(start.Registrieren())  //Wert Ausgabe

```