package controller

import (
	"crypto/md5"
	"encoding/hex"
	"github.com/spf13/viper"
	"github.com/thorstenkloehn/mitgliederverwaltung/model"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type Mitglieder struct {
	Mitglieder   model.Mitglider
	Rohepasswort []byte
}

func (start *Mitglieder) Index() *[]model.Mitglider {

	dsn := viper.GetString("mysql_nutzername") + `:` + viper.GetString("mysql_passwort") + `@tcp(127.0.0.1:3306)/` + viper.GetString("Mysql_Datenbank_Name") + `?charset=utf8mb4&parseTime=True&loc=Local`
	db, _ := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	var mitglieder []model.Mitglider
	db.Find(&mitglieder)
	return &mitglieder

}

func (start *Mitglieder) Registrieren() string {
	dsn := viper.GetString("mysql_nutzername") + `:` + viper.GetString("mysql_passwort") + `@tcp(127.0.0.1:3306)/` + viper.GetString("Mysql_Datenbank_Name") + `?charset=utf8mb4&parseTime=True&loc=Local`
	db, _ := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	resultat := db.Where("user = ?", start.Mitglieder.User).First(&start.Mitglieder)

	if resultat.RowsAffected == 0 {
		resultat1 := db.Where("emails = ?", start.Mitglieder.Emails).First(&start.Mitglieder)
		if resultat1.RowsAffected == 0 {
			data := start.Rohepasswort
			hasher := md5.New()
			hasher.Write((data))
			start.Mitglieder.Passwort = hex.EncodeToString(hasher.Sum(data))
			db.Create(&start.Mitglieder)
			return "Neu Nutzer hergestellt"
		} else {
			return "Emails ist vorhanden"
		}

	} else {
		return "User Name ist Vorhanden oder Emails Vorhanden"
	}

}

func (start *Mitglieder) Einzelne_Mitglied(id *uint64) model.Mitglider {
	dsn := viper.GetString("mysql_nutzername") + `:` + viper.GetString("mysql_passwort") + `@tcp(127.0.0.1:3306)/` + viper.GetString("Mysql_Datenbank_Name") + `?charset=utf8mb4&parseTime=True&loc=Local`
	db, _ := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	var mitgliederEinzeln model.Mitglider
	db.Find(&mitgliederEinzeln, id)
	return mitgliederEinzeln
}
