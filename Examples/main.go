package main

import (
	"fmt"
	"github.com/spf13/viper"
	"github.com/thorstenkloehn/mitgliederverwaltung"
)

func main() {

	viper.SetConfigName("config")  // Name der Konfigurationsdatei (ohne Erweiterung)
	viper.SetConfigType("yaml")    // ERFORDERLICH, wenn die Konfigurationsdatei die Erweiterung nicht im Namen hat
	viper.AddConfigPath("config/") // Suchen Sie optional nach config im Arbeitsverzeichnis
	viper.AutomaticEnv()
	viper.SetDefault("Mysql_Nutzername", "root")
	viper.SetDefault("Mysql_Passwort", "Test")
	viper.SetDefault("Mysql_Datenbank_Name", "start")

	err := viper.ReadInConfig() // Suchen und lesen Sie die Konfigurationsdatei
	if err != nil {             // Handle errors reading the config file
		panic(fmt.Errorf("Fatal error config file: %w \n", err))
	}
	mitgliederverwaltung.Installieren()

	// var start = controller.Mitglieder{}

}
